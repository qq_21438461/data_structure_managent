import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Dialogs
import QtQuick.Layouts 1.15

Item {
    id: colorSettingPage
    anchors.fill: parent

    signal colorSelected(string color)
    property color currentColor: "black"

    Column {
        anchors.centerIn: parent
        spacing: 10
        width: 280
        Text {
            text: "颜色设置"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        RowLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 10
            anchors.fill: parent

            SettingItem {
                id: selectColorButton
                title: "选择颜色"
                onClicked: {
                    colorDialog.open();
                }
            }

            Rectangle {
                id: colorPreview
                width: 40
                height: 40
                radius: 10
                color: colorSettingPage.currentColor
                border.color: "white"
                border.width: 1
            }
        }

        SettingItem {
            id: setColorButton
            title: "设置"
            onClicked: {
                colorSettingPage.colorSelected(colorSettingPage.currentColor);
                stackView.pop();
            }
        }

        SettingItem {
            id: cancelButton
            title: "返回"
            onClicked: {
                stackView.pop();
            }
        }
    }

    ColorDialog {
        id: colorDialog
        title: "选择颜色"
        onAccepted: {
            colorSettingPage.currentColor = colorDialog.color;
            colorPreview.color = colorDialog.color;
        }
    }
}
