import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Shapes 1.15

Rectangle {
    id: outerRectangle
    property int borderWidth: 5
    property color borderColor1: "#4a90e2"
    property color borderColor2: "#00a8ff"
    property color innerBackgroundColor: "gray"
    property int borderRadius: 10

    radius: outerRectangle.borderRadius

    gradient: LinearGradient {
        x1: 0; y1: 0
        x2: 0; y2: 1
        GradientStop { position: 0.0; color: outerRectangle.borderColor1 }
        GradientStop { position: 1.0; color: outerRectangle.borderColor2 }
    }

    Rectangle {
        id: innerRectangle
        anchors.fill: parent
        anchors.margins: outerRectangle.borderWidth
        radius: outerRectangle.radius - outerRectangle.borderWidth
        color: outerRectangle.innerBackgroundColor

        // Your inner content here
    }
}
