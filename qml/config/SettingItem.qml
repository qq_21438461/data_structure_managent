import QtQuick 2.15

BorderTemplate {
    id: settingItem
    width: parent.width
    height: 40
    borderWidth: 1
    borderColor1: "red"
    borderColor2: "red"
    innerBackgroundColor: "black"
    borderRadius: 10

    signal clicked

    Text {
        id: settingTitle
        color: "white"
        anchors.centerIn: parent
        font.pixelSize: 28 // 设置字体大小为 32 像素
        font.family: "楷体"
        font.bold: true
        text: settingItem.title // 使用设置项的 'title' 属性
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: settingItem.clicked()
    }

    property string title: "" // 在 Rectangle 上添加 'title' 属性
}

