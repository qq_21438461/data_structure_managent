import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Shapes 1.15

Popup {
    id: settingsDialog
    width: 300
    height: 400
    modal: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

    background: BorderTemplate {
        width: parent.width
        height: parent.height
        borderWidth: 5
        borderColor1: "#4a90e2"
        borderColor2: "#00a8ff"
        innerBackgroundColor: "gray"
        borderRadius: 20
    }



    Column {
        anchors.centerIn: parent
        width: 280
        height: 380
        spacing: 10

        Rectangle {
            width: parent.width
            height: 40
            color: "transparent"

            Text {
                text: "设置"
                font.pixelSize: 20
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
            }
        }

        Rectangle {
            width: parent.width
            height: parent.height - 40
            color: "transparent"
            clip: true

            StackView {
                id: stackView
                anchors.fill: parent
                anchors.margins: 10

                initialItem: Column {
                    anchors.fill: parent
                    anchors.margins: 10
                    spacing: 10

                    SettingItem {
                        id: colorSetting
                        title: "颜色设置"
                        onClicked: stackView.push("ColorSetting.qml")
                    }

                    // 其他设置项
                }
            }
        }
    }
}
