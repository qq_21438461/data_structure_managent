import QtQuick 2.15

Item {
    id: root
    implicitWidth: 50
    implicitHeight: 50
    property int itemIndex
    property color nodeColor: "lightblue"
    signal clicked(int itemIndex)

    MouseArea {
        anchors.fill: parent
        onClicked: {
            root.clicked(itemIndex);
        }
    }

    Canvas {
        id: canvas
        anchors.fill: parent  // 将 Canvas 锚点设置为填充父项

        onPaint: {
            var ctx = getContext('2d');  // 获取 2D 绘图上下文
            ctx.reset();  // 重置绘图上下文的状态

            // 绘制圆形的过程开始
            ctx.beginPath();  // 创建一个新的路径
            ctx.arc(width / 2, height / 2, width / 2 - ctx.lineWidth, 0, 2 * Math.PI);  // 添加一个圆形路径
            ctx.fillStyle = nodeColor;  // 设置填充颜色为浅蓝色
            ctx.fill();  // 填充当前路径
            ctx.strokeStyle = 'black';  // 设置描边颜色为黑色
            ctx.stroke();  // 绘制当前路径的描边
            ctx.closePath();  // 关闭当前路径
            // 绘制圆形的过程结束
        }
    }

}

