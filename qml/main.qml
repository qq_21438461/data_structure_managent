import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Scene2D 2.15
import QtQuick.Dialogs
import "config/"


ApplicationWindow {
    id: mainWindow
    visible: true // 设置窗口可见
    width: 640 // 设置窗口宽度
    height: 480 // 设置窗口高度
    title: qsTr("Circular Queue Visualization") // 设置窗口标题


    Rectangle { // 定义场景
        id: scene // 为场景设置 ID
        anchors.fill: parent // 设置场景填充整个窗口
        focus: true // 使场景具有焦点

        signal itemClicked(int itemIndex) // 定义一个信号，当项目被点击时发送

        function handleItemClicked(itemIndex) { // 定义一个处理点击事件的函数
            message.text = "Clicked item: " + itemIndex; // 更新 message 文本
        }

        Component.onCompleted: { // 当场景创建完成后执行此代码块
            for (var i = 0; i < 5; i++) { // 遍历5个节点
                createNode(i); // 创建节点
            }
        }

        function createNode(index) { // 定义一个创建节点的函数
            var node = Qt.createComponent("Node.qml"); // 从 "Node.qml" 创建组件
            var nodeObject = node.createObject(scene, {"itemIndex": index}); // 创建节点对象并设置 itemIndex 属性
            nodeObject.x = width / 2 + 200 * Math.cos(index * 2 * Math.PI / 5 - Math.PI / 2) - nodeObject.width / 2; // 设置节点的 x 坐标
            nodeObject.y = height / 2 + 200 * Math.sin(index * 2 * Math.PI / 5 - Math.PI / 2) - nodeObject.height / 2; // 设置节点的 y 坐标
            nodeObject.clicked.connect(handleItemClicked); // 将节点的 clicked 信号连接到 handleItemClicked 函数
            nodePositions.push(Qt.point(nodeObject.x + nodeObject.width / 2, nodeObject.y + nodeObject.height / 2)); // 将节点中心位置添加到 nodePositions 数组
        }
    }
    // 声明一个组件以创建 SettingsDialog 实例
    Component {
        id: settingsDialogComponent
        SettingsDialog {
            id: settingsDialog // 修改这里，将 id 更改为 settingsDialog
        }
    }

    Image {
        id: settingsButton
        visible: true // 设置窗口可见
  //      source: "file:///D:/work/code/git/data_structure_managent/resources/png/set/Setting.png"
        source: "qrc:/image/resources/png/set/Setting.png" // 使用相对路径
         width: 50; height: 50
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 20
        Component.onCompleted: {
            console.log("Image component created.");
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                var settingsDialogInstance = settingsDialogComponent.createObject(mainWindow.contentItem);
                settingsDialogInstance.open();

            }
        }



    }

    Text { text: settingsButton.status == settingsButton.Ready ? 'Loaded' : 'Not loaded' }
    Text { // 定义文本
        id: message // 为文本设置 ID
        anchors.bottom: parent.bottom // 将文本底部锚定到窗口底部
        anchors.left: parent.left // 将文本左侧锚定到窗口左侧
        anchors.right: parent.right // 将文本右侧锚定到窗口右侧
        anchors.margins: 20 // 设置文本的边距
        font.pixelSize: 24 // 设置字体大小
        horizontalAlignment: Text.AlignHCenter // 设置水平对齐方式为居中
        text: qsTr("Click an item to view its index.") // 设置文本内容
    }

    property var nodePositions: [] // 定义一个属性，用于存储节点位置

    Canvas { // 定义画布
        id: canvas // 为画布设置 ID
        anchors.fill: parent // 设置画布填充整个窗口

        onPaint: { // 定义绘制事件处理器
            var ctx = getContext("2d"); // 获取 2D 绘图上下文
            ctx.reset(); // 重置上下文状态
            ctx.strokeStyle = "black"; // 设置线条颜色为黑色
            ctx.lineWidth = 2; // 设置线条宽度为 2 像素

            for (var i = 0; i < nodePositions.length; i++) { // 遍历节点位置数组
                var startPos = nodePositions[i]; // 获取当前节点位置
                var endPos = nodePositions[(i + 1) % nodePositions.length]; // 获取下一个节点位置，使用取余运算实现循环连接
                ctx.beginPath(); // 开始新的路径
                ctx.moveTo(startPos.x, startPos.y); // 将绘图光标移动到当前节点位置
                ctx.lineTo(endPos.x, endPos.y); // 绘制一条线到下一个节点位置
                ctx.stroke(); // 描绘线条
            }

            ctx.fillStyle = "red"; // 设置填充颜色为红色
            ctx.font = "bold 16px sans-serif"; // 设置字体样式
            ctx.textAlign = "center"; // 设置文本对齐方式为居中
            ctx.fillText("Head", nodePositions[0].x, nodePositions[0].y - 30); // 在第一个节点上方绘制 "Head" 文本
            ctx.fillText("Tail", nodePositions[nodePositions.length - 1].x, nodePositions[nodePositions.length - 1].y - 30); // 在最后一个节点上方绘制 "Tail" 文本
        }
    }

}
