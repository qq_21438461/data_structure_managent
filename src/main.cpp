#include "CircularBufferComm.hpp"
#include "QueueComm.hpp"
#include "MapComm.hpp"
#include <iostream>
#include <string>
#include <cstring>
#include <chrono>
#include <QFile>
#include <QIODevice>
#include <QTextStream>
#include <QByteArray>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
int main(int argc, char *argv[])
{

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/DataManager/qml/main.qml"));

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
