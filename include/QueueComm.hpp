
/**
 * @file QueueComm.hpp
 * @author liuzhiyu (liuzhiyu@inalfa-acms.com)
 * @brief This header file includes the definition of the CircularBuffer class,
 *        a thread-safe, fixed-size, circular buffer implementation with
 * optional duplicate checks. It also includes templates for thread
 * communication interfaces.
 * @version V1.0.0
 * @date 2023-12-11
 * @copyright Copyright (c) 2023 by liuzhiyu, All Rights Reserved.
 */
#pragma once
#include <algorithm>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <queue>
#include <thread>

/**
 * @class SafeQueue
 * @brief A thread-safe, dynamic-size, queue implementation for concurrent
 * access and processing.
 *
 * This class provides a thread-safe queue implementation with the following
 * features:
 * - Dynamic-size queue that can grow and shrink as needed.
 * - Enqueue and enqueue_move methods for adding elements to the queue.
 * - Thread-safe dequeue and try_dequeue methods for removing elements from the
 * queue.
 * - Utility methods for checking queue size and emptiness.
 * - Thread-safe clear method to reset the queue state.
 *
 * @tparam T The type of elements stored in the queue.
 */
template <typename T>
class SafeQueue {
 public:
  /* 目前没有动态调整策略的需求，暂时采用静态策略 ，后续有需要再添加动态调整策略
   */
  static constexpr bool AllowDuplicates = true;  // 是否允许缓冲区中的元素重复
  static constexpr bool LimitSize = false;       // 是否限制缓冲区大小
  static constexpr bool Replace_oldest = false;  // 满时是否替换最旧的元素

  using value_type = T;  // 添加 value_type 定义，以便在其他地方使用

  SafeQueue(size_t capacity = 0) : m_capacity(capacity) {}
  ~SafeQueue() = default;
  /**
   * @brief 向队列添加元素（线程安全）
   *
   * 本函数接受一个指针类型的参数，并尝试将其指向的元素添加到队列中。
   * 如果传入的指针为空（nullptr），则不会尝试添加，函数返回 false。
   * 使用模板函数 _push_internal_ 实现添加操作。
   *
   * @param t 要添加的元素的指针类型。
   * @return 如果成功添加元素，返回 true；否则返回 false
   */
  bool push(T* t) {
    if (t == nullptr) {
      return false;
    }
    return _push_internal_(*t);
  }
  /**
   * @brief 向队列添加元素（线程安全）
   *
   * 本函数接受一个模板类型的参数，并尝试将其添加到队列中。
   * 参数可以是左值引用或右值引用。
   * 使用模板函数 _push_internal_ 实现添加操作。
   *
   * @tparam U 元素类型
   * @param t 要添加的元素（右值引用）
   * @return 如果成功添加元素，返回 true；否则返回 false
   */
  template <typename U>
  bool push(U&& t) {
    return _push_internal_(std::forward<U>(t));
  }
  /**
   * @brief 向队列中原地构造并添加元素（线程安全）
   *
   * 本函数接受任意数量和类型的参数，使用这些参数在队列的末尾原地构造元素。
   * 使用可变参数模板 (variadic templates) 和完美转发来实现高效的参数传递。
   * 调用内部函数 _push_internal_ 来实际进行添加操作。
   *
   * @tparam Args 构造元素所需参数的类型
   * @param args 用于构造元素的参数列表
   * @return 如果成功构造并添加元素到队列，返回 true；否则返回 false
   */
  template <typename... Args>
  bool emplace(Args&&... args) {
    T t(std::forward<Args>(args)...);
    return _push_internal_(std::move(t));
  }
  /**
   * 尝试从队列中弹出一个元素。
   * @decide: 该函数将立即返回，不会等待。
   * @param t 引用，用于保存弹出的元素
   * @return 如果成功弹出元素，返回 true；否则返回 false
   */
  bool pop(T& t) {
    return _pop_internal_(t, std::chrono::microseconds::zero());
  }
  /**
   * 尝试等待并弹出队列中的元素。
   * 如果在给定的超时时间内队列为空，则返回 false，否则返回 true
   * 并将弹出的元素存储在 t 中。
   *
   * @tparam Rep 表示时间的数量类型。
   * @tparam Period 时间单位类型。
   * @param[out] t 用于存储弹出的元素。
   * @param[in] timeout 等待队列非空的超时时间。默认值为 -1，表示无限等待。
   * @return 如果在超时时间内成功弹出元素，则返回 true，否则返回 false。
   */
  template <typename Duration = std::chrono::microseconds>
  bool wait_and_pop(T& t,
                    const Duration& timeout = std::chrono::microseconds(-1)) {
    return _pop_internal_(t, timeout);
  }

  // 检查队列是否为空
  bool empty() const {
    std::unique_lock<std::mutex> lock(m_mutex);
    return m_queue.empty();
  }
  // 获取队列大小
  size_t size() const {
    std::unique_lock<std::mutex> lock(m_mutex);
    return m_queue.size();
  }
  // 清空队列
  void clear() {
    std::lock_guard<std::mutex> lock(m_mutex);
    while (!m_queue.empty()) {
      m_queue.pop();
    }
  }
  // 设置队列容量
  void set_capacity(size_t capacity) {
    std::unique_lock<std::mutex> lock(m_mutex);
    m_capacity = capacity;
    while (m_queue.size() > m_capacity) {
      m_queue.pop();
    }
  }

 private:
  /**
   * @brief 内部函数，向队列中添加元素（线程安全）
   *
   * 该函数尝试将元素添加到队列中。如果队列容量非零且队列已满，函数将返回
   * false。 如果不允许重复元素且元素已存在于队列中，函数也将返回 false。
   * 使用互斥锁保证线程安全，并在添加成功后通知其他等待线程。
   *
   * @tparam U 元素类型
   * @param t 要添加的元素（右值引用）
   * @return 如果成功添加元素，返回 true；否则返回 false
   */
  template <typename U>
  bool _push_internal_(U&& t) {
    std::unique_lock<std::mutex> lock(m_mutex);

    if constexpr (LimitSize) {
      // 如果队列容量非零且队列已满，则返回 false
      if (m_capacity > 0 && m_queue.size() >= m_capacity) {
        return false;
      }
    }
    if constexpr (!AllowDuplicates) {
      // 如果不允许重复元素且元素已存在于队列中，则返回 false
      if (std::find(m_queue.cbegin(), m_queue.cend(), t) != m_queue.cend()) {
        return false;
      }
    }
    // 将元素添加到队列中
    if constexpr (std::is_pointer<U>::value) {
      m_queue.push(*t);
    } else {
      m_queue.push(std::forward<U>(t));
    }
    lock.unlock();      // 释放锁
    m_cv.notify_one();  // 通知其他线程可以继续操作队列
    return true;
  }
  /**
   * @brief 内部函数，尝试等待并弹出队列中的元素（线程安全）
   * 如果在给定的超时时间内队列为空，则返回 false，否则返回 true
   * 并将弹出的元素存储在 t 中,并通知其他线程可以继续入队。
   *
   * @param[out] t 用于存储弹出的元素。
   * @param[in] timeout 等待队列非空的超时时间
   * @return 如果成功从队列中弹出元素，则返回 true，否则返回 false
   *
   * 当 timeout 为零时，函数将立即尝试获取数据，不会等待。
   * 当 timeout 大于零时，函数将最多尝试等待指定的超时时间。
   * 如果在超时时间内循环缓冲区中仍无元素可弹出，函数将立即返回 false。
   */
  template <typename Duration = std::chrono::microseconds>
  bool _pop_internal_(T& t, const Duration& timeout) {
    std::unique_lock<std::mutex> lock(m_mutex);
    // 首先检查队列是否已经非空
    if (m_queue.empty()) {
      // 根据超时设置等待
      if (timeout.count() < 0) {
        m_cv.wait(lock, [this]() { return !m_queue.empty(); });
      } else if (timeout.count() == 0) {
        return false;
      } else if (!m_cv.wait_for(lock, timeout,
                                [this]() { return !m_queue.empty(); })) {
        return false;
      }
    }
    t = std::move(m_queue.front());
    m_queue.pop();
    return true;
  }
  std::queue<T> m_queue;
  size_t m_capacity = 0;  // 允许的队列大小
  std::condition_variable m_cv;
  mutable std::mutex m_mutex;
};

