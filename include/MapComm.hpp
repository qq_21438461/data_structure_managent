/**
 * @file MapComm.hpp
 * @author liuzhiyu (liuzhiyu@inalfa-acms.com)
 * @brief This file defines the SafeMap class.
 * @date 2023-12-15
 * @version 1.0.0
 * @copyright Copyright (c) 2023 by liuzhiyu, All Rights Reserved.
 */
#pragma once
#include <map>
#include <mutex>
#include <shared_mutex>
#include <unordered_map>
#include <utility>

template <typename Key, typename Value,
          template <typename, typename, typename...> class Map = std::map>
class SafeMap {
 public:
  using map_type = Map<Key, Value>;

  SafeMap() = default;
  ~SafeMap() = default;

  // 添加或更新元素
  void insert_or_assign(const Key& key, const Value& value) {
    std::unique_lock lock(m_mutex);
    m_map[key] = value;
  }

  // 检索元素
  bool get(const Key& key, Value& value) const {
    std::shared_lock lock(m_mutex);
    auto it = m_map.find(key);
    if (it != m_map.end()) {
      value = it->second;
      return true;
    }
    return false;
  }

  // 移除元素
  bool erase(const Key& key) {
    std::unique_lock lock(m_mutex);
    return m_map.erase(key) > 0;
  }

  // 检查是否为空
  bool empty() const {
    std::shared_lock lock(m_mutex);
    return m_map.empty();
  }

  // 获取大小
  size_t size() const {
    std::shared_lock lock(m_mutex);
    return m_map.size();
  }

  // 清空映射
  void clear() {
    std::unique_lock lock(m_mutex);
    m_map.clear();
  }

 private:
  mutable std::shared_mutex m_mutex;
  map_type m_map;
};
