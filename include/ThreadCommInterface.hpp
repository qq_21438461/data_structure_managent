/**
 * @file OSA_ThreadCommInterface.hpp
 * @author liuzhiyu (liuzhiyu@inalfa-acms.com)
 * @brief This header file defines a collection of thread communication interface templates for MID.
 *        It offers a suite of thread-safe operations for manipulating various types of containers, 
 *        including pushing, popping, element management, and querying container states.
 *        The interfaces are designed to abstract the complexity of thread synchronization while 
 *        performing common container operations, ensuring safe and efficient communication 
 *        across different threads.
 *        
 *        The file contains templates for:
 *        - Pushing elements into various container types (Push, Push for pointer types).
 *        - Creating new elements directly within containers (Emplace).
 *        - Popping elements from containers with options for timed waiting (Pop, WaitAndPop).
 *        - Inserting or assigning values in map containers (InsertOrAssign).
 *        - Retrieving elements from map containers with safety checks (Get).
 *        - Erasing elements from map containers (Erase).
 *        - Checking container states, like emptiness or size (Empty, Size).
 *        - Clearing all elements from a container (Clear).
 *        
 *        These functions are templated to be versatile and adaptable to various container types,
 *        including custom container implementations that follow standard interface patterns.
 *        The functions rely on the existence of specific member functions within the container types,
 *        ensuring a consistent and predictable behavior across different container implementations.
 *
 * @version V1.0.0
 * @date 2023-12-14
 *
 * Usage Example:
 * 
 * This section demonstrates how to use the thread communication interface templates with various container types.
 * 
 * Example 1: Using Push and Pop with a Custom Queue Container
 * ```
 * OSA_QueueComm<int> myQueue;
 * osa::threadcomm::Push(myQueue, 42);         // Push an element into the queue
 * int poppedValue;
 * if (osa::threadcomm::Pop(myQueue, poppedValue)) {   // Pop an element from the queue
 *     std::cout << "Popped Value: " << poppedValue << std::endl;
 * }
 * ```
 * 
 * Example 2: Using Emplace with a Custom List Container
 * ```
 * OSA_ListComm<std::string> myList;
 * osa::threadcomm::Emplace(myList, "Hello, World!");  // Create and add an element directly in the list
 * ```
 * 
 * Example 3: Using WaitAndPop with Timeout on a Queue Container
 * ```
 * OSA_QueueComm<std::string> myStringQueue;
 * std::string strValue;
 * if (osa::threadcomm::WaitAndPop(myStringQueue, strValue, std::chrono::milliseconds(100))) {
 *     std::cout << "Received: " << strValue << std::endl;
 * } else {
 *     std::cout << "No element popped within 100 milliseconds." << std::endl;
 * }
 * ```
 * 
 * Example 4: Using InsertOrAssign and Get with a Custom Map Container
 * ```
 * OSA_MapComm<int, std::string> myMap;
 * osa::threadcomm::InsertOrAssign(myMap, 1, "One");
 * std::string value;
 * if (osa::threadcomm::Get(myMap, 1, value)) {
 *     std::cout << "Key 1: " << value << std::endl;
 * }
 * ```
 * 
 * These examples showcase basic usage patterns of the thread communication templates with different container types.
 * They demonstrate how these templates can be used to perform thread-safe operations on containers such as queues, lists, and maps.
 * 
 *
 * @note The functionality and efficiency of these interfaces depend on the specific implementations
 *       of the member functions in the provided container types.
 * 
 * @copyright Copyright (c) 2023 by liuzhiyu, All Rights Reserved.
 *
 */

#pragma once
#include <chrono>
#include <type_traits>
#include <utility>

#include "CircularBuffeComm.hpp"
#include "ListComm.hpp"
#include "MapComm.hpp"
#include "QueueComm.hpp"

/**
 * Pushes an element into the container.
 * 
 * @tparam Container The type of the container.
 * @tparam U The type of the element to be pushed.
 * @param container The container to push the element into.
 * @param t The element to be pushed.
 * @return The result of the push operation.
 */
template <typename Container, typename U,
          typename = std::enable_if_t<std::is_invocable_v<
              decltype(&Container::template push<U>), Container&, U&&>>>
auto Push(Container& container, U&& t) {
  return container.push(std::forward<U>(t));
}
/**
 * Pushes a pointer element into the container.
 * 
 * @tparam Container The type of the container.
 * @tparam T The type of the pointer element to be pushed.
 * @param container The container to push the element into.
 * @param value The pointer element to be pushed.
 * @return The result of the push operation.
 */
template <typename Container, typename T,
          typename = std::enable_if_t<
              std::is_invocable_v<decltype(&Container::push), Container&, T*>>>
auto Push(Container& container, T* value) {
  return container.push(value);
}
/**
 * Creates a new element in the container.
 * 
 * @tparam Container The type of the container.
 * @tparam Args The types of the arguments to construct the element.
 * @param container The container to create the element in.
 * @param args The arguments to construct the element.
 * @return The result of the emplace operation.
 */
template <typename Container, typename... Args,
          typename = std::enable_if_t<std::is_invocable_v<
              decltype(&Container::template emplace<Args...>), Container&,
              Args&&...>>>
auto Emplace(Container& container, Args&&... args) {
  return container.emplace(std::forward<Args>(args)...);
}
/**
 * Pops an element from the container.
 * 
 * @tparam Container The type of the container.
 * @param container The container to pop the element from.
 * @param value Reference to store the popped element.
 * @return The result of the pop operation.
 */
template <typename Container, typename = std::enable_if_t<std::is_invocable_v<
                                  decltype(&Container::pop), Container&,
                                  typename Container::value_type&>>>
auto Pop(Container& container, typename Container::value_type& value) {
  return container.pop(value);
}
/**
 * Attempts to wait and pop an element from the container within a given timeout.
 * If the container is empty within the timeout duration, returns false;
 * otherwise, returns true and stores the popped element in `value`.
 *
 * @tparam Container The type of the container.
 * @tparam value_type The type of the element in the container.
 * @param container The container to pop the element from.
 * @param[out] value Reference to store the popped element.
 * @param[in] timeout The timeout duration for waiting for a non-empty container. 
 *                    A default value of -1 indicates indefinite waiting.
 * @return True if an element is successfully popped within the timeout, false otherwise.
 */
template <typename Container>
auto WaitAndPop(Container& container, typename Container::value_type& value,
                const std::chrono::milliseconds timeout = std::chrono::milliseconds(-1)) {
    return container.wait_and_pop(value, timeout);
}
/**
 * Inserts or assigns a value to a map container.
 * 
 * @tparam Map The type of the map container.
 * @tparam Key The type of the key.
 * @tparam Value The type of the value.
 * @param map The map container.
 * @param key The key where the value will be inserted or assigned.
 * @param value The value to be inserted or assigned.
 * @return The result of the insert_or_assign operation.
 */
template <typename Map, typename Key, typename Value>
auto InsertOrAssign(Map& map, const Key& key, Value&& value) {
  return map.insert_or_assign(key, std::forward<Value>(value));
}
/**
 * Retrieves an element from the map container.
 * 
 * @tparam Map The type of the map container.
 * @tparam Key The type of the key.
 * @tparam Value The type of the value to be retrieved.
 * @param map The map container.
 * @param key The key of the element to retrieve.
 * @param[out] value Reference to store the retrieved element.
 * @return True if the element is found and retrieved, false otherwise.
 */
template <typename Map, typename Key, typename Value>
bool Get(const Map& map, const Key& key, Value& value) {
  return map.get(key, value);
}

/**
 * Erases an element from the map container.
 * 
 * @tparam Map The type of the map container.
 * @tparam Key The type of the key.
 * @param map The map container.
 * @param key The key of the element to erase.
 * @return True if an element is successfully erased, false otherwise.
 */
template <typename Map, typename Key,
          typename = std::enable_if_t<
              std::is_invocable_v<decltype(&Map::erase), Map&, const Key&>>>
bool Erase(Map& map, const Key& key) {
  return map.erase(key) > 0;
}

/**
 * Checks if the container is empty.
 * 
 * @tparam Container The type of the container.
 * @param container The container to check.
 * @return True if the container is empty, false otherwise.
 */
template <typename Container,
          typename = std::enable_if_t<std::is_invocable_v<
              decltype(&Container::empty), const Container&>>>
auto Empty(const Container& container) {
  return container.empty();
}

/**
 * Returns the size of the container.
 * 
 * @tparam Container The type of the container.
 * @param container The container whose size is to be determined.
 * @return The size of the container.
 */
template <typename Container,
          typename = std::enable_if_t<std::is_invocable_v<
              decltype(&Container::size), const Container&>>>
auto Size(const Container& container) {
  return container.size();
}

/**
 * Clears all elements from the container.
 * 
 * @tparam Container The type of the container.
 * @param container The container to clear.
 */
template <typename Container, typename = std::enable_if_t<std::is_invocable_v<
                                  decltype(&Container::clear), Container&>>>
void Clear(Container& container) {
  container.clear();
}

