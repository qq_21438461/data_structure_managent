/**
 * @file OSA_ThreadPool.h
 * @author liuzhiyu (liuzhiyu@inalfa-acms.com)
 * @brief This header file includes the definition of the ThreadWrapper class, a utility class for enhanced thread management.
 * @version V1.0.0
 * @date 2023-12-11
 * @copyright Copyright (c) 2023 by liuzhiyu, All Rights Reserved.
 */
#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>
#include "OSA_ThreadWrapper.h"
#if (__cplusplus >= 201703L) 
  #if (defined(__GNUC__) && (__GNUC__ > 7)) || (defined(_MSC_VER) && (_MSC_VER >= 1910))
    #define USE_CPP17_FEATURES
    #include <tuple>
  #endif
#endif
namespace osa {
namespace ThreadModule {
/**
 *@class ThreadPool
 * @brief A ThreadPool class for managing a pool of threads.
 * 
 * This class provides a flexible pool of threads that can execute any callable function including 
 * lambda expressions, bound functions, or any function objects. It allows for parallel execution 
 * of tasks with improved performance and efficiency. Tasks can be added to the pool dynamically, 
 * and they are executed as soon as a thread becomes available. The ThreadPool manages the lifecycle 
 * of the threads, creating and destroying them as needed, up to a specified maximum limit.
 * 
 * The ThreadPool is especially useful in scenarios where tasks are numerous and short-lived, 
 * or for server-like applications where tasks arrive intermittently and need to be processed 
 * in parallel.
 * 
 * Example usage:
 *     ThreadPool pool(4); // Create a ThreadPool with 4 threads
 *     auto result = pool.enqueue([](int answer) { return answer; }, 42);
 *     std::cout << result.get(); // prints 42
 * 
 * @note The ThreadPool class is designed to be thread-safe. It uses mutexes and condition 
 *       variables to synchronize access to its task queue.
 */
class ThreadPool {
public:
    /***************************************************************
     *  @note      ThreadPool constructor
     *  @param     maxThreads Maximum number of threads in the pool
     ***************************************************************/
    ThreadPool(size_t maxThreads);
    /**
     * @brief Destructor for ThreadPool.
     * @note Joins all threads before destruction.
     */
    ~ThreadPool();
/**
 * @brief Add task to the thread pool.
 * 
 * @tparam FunctionType Type of the function to be added.
 * @tparam Args Types of the arguments to pass to the function.
 * @param f Function to be added to the pool.
 * @param args Arguments to pass to the function.
 * @return std::future representing the task's return value.
 * 
 * @note This function adds a new task to the thread pool and returns a future 
 *       representing the outcome of this task.
 */
#ifdef USE_CPP17_FEATURES
    template<class F, class... Args>
    auto enqueue(F&& f, Args&&... args) 
        -> std::future<std::invoke_result_t<F, Args...>>
    {
        using return_type = std::invoke_result_t<F, Args...>;
    
        // 将参数捆绑到一个tuple中
        auto bound_args = std::make_tuple(std::forward<Args>(args)...);
    
        auto task = std::make_shared<std::packaged_task<return_type()>>(
                [f = std::forward<F>(f), bound_args]() mutable {
                    // 使用std::apply来解包tuple并调用函数
                    return std::apply(f, bound_args);
                }
            );
#else
    template<class F, class... Args>
    auto enqueue(F&& f, Args&&... args) 
        -> std::future<typename std::result_of<F(Args...)>::type>
    {
        using return_type = typename std::result_of<F(Args...)>::type;

        auto task = std::make_shared< std::packaged_task<return_type()> >(
                std::bind(std::forward<F>(f), std::forward<Args>(args)...)
            );
#endif    
        std::future<return_type> res = task->get_future();
    
        // 加入任务到队列
        {
            std::unique_lock<std::mutex> lock(queue_mutex);
    
            // 不允许在停止线程池后加入新的任务
            if(stop) {
                throw std::runtime_error("enqueue on stopped ThreadPool");
            }
    
            tasks.emplace([task](){ (*task)(); });
        }
    
        condition.notify_one();
    
        return res;
    }
    
    /**
     * @brief Get the number of working threads in the current thread pool.
     * @return Number of working threads.
     */
    size_t getWorkingThreadCount() {
        return workingThreads.load();
    }
private:
    /**
     * @brief Vector of thread wrappers.
     * 
     * Workers are responsible for executing the tasks in the pool.
     */
    std::vector<std::unique_ptr<ThreadWrapper>> workers;

    /**
     * @brief Queue of tasks.
     * 
     * Tasks are functions that are added to the thread pool to be executed.
     */
    std::queue<std::function<void()>> tasks;

    /**
     * @brief Atomic variable representing the number of working threads.
     * 
     * This variable keeps track of the number of threads currently executing tasks.
     */
    std::atomic<size_t> workingThreads{0};

    /**
     * @brief Mutex for synchronizing access to the task queue.
     */
    std::mutex queue_mutex;

    /**
     * @brief Condition variable used to notify worker threads about available tasks.
     */
    std::condition_variable condition;

    /**
     * @brief Flag to indicate whether the thread pool is stopped.
     * 
     * When set to true, no new tasks can be added to the pool, and the pool will begin shutting down.
     */
    bool stop;

    /**
     * @brief Maximum number of threads allowed in the pool.
     */
    size_t maxThreads;
};
}  // namespace ThreadModule
}  // namespace osa


#endif
