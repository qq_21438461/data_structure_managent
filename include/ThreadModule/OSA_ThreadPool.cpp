
#include "OSA_ThreadPool.h"
namespace osa {
namespace ThreadModule {
ThreadPool::ThreadPool(size_t maxThreads) : stop(false), maxThreads(maxThreads) {
  for (size_t i = 0; i < maxThreads; ++i) {
      workers.emplace_back(new ThreadWrapper);
      workers.back()->setmain([this] {
          for(;;) {//The thread function cannot exit, otherwise it will exit the thread.
              std::function<void()> task;
              {
                  std::unique_lock<std::mutex> lock(this->queue_mutex);
                  this->condition.wait(lock, [this]{ return this->stop || !this->tasks.empty(); });
                  if(this->stop && this->tasks.empty()) {
                      return;
                  }
                  task = std::move(this->tasks.front());
                  this->tasks.pop();
              }
              ++workingThreads;
              task();
              --workingThreads;
          }
      });
      workers.back()->start();
  }
}
ThreadPool::~ThreadPool(){
  {
      std::unique_lock<std::mutex> lock(queue_mutex);
      stop = true;
  }
  condition.notify_all();
  for(auto &worker : workers) {
      worker->stop();
  }
}
}
}