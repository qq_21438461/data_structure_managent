/**
 * @file CircularBufferComm.hpp
 * @author liuzhiyu (liuzhiyu@inalfa-acms.com)
 * @brief This header file includes the definition of the CircularBuffer class,
 *        a thread-safe, fixed-size, circular buffer implementation with
 * optional duplicate checks. It also includes templates for thread
 * communication interfaces.
 * @version V1.0.0
 * @date 2023-12-11
 * @copyright Copyright (c) 2023 by liuzhiyu, All Rights Reserved.
 */
#pragma once
#include <algorithm>
#include <chrono>
#include <condition_variable>
#include <cstdlib>
#include <iostream>
#include <mutex>
#include <new>
#include <type_traits>
#include <vector>
#include <malloc.h> // 对于 _aligned_malloc 和 _aligned_free
#include <cstdlib>

#define allow_duplicates_switch 1

/**
 * @class CircularBuffer
 * @brief A thread-safe, fixed-size, circular buffer implementation with
 * optional duplicate checks.
 *
 * This class provides a thread-safe, circular buffer implementation with the
 * following features:
 * - Fixed-size buffer with optional dynamic resizing.
 * - Push and emplace methods for adding elements to the buffer.
 * - Thread-safe pop and wait_and_pop methods for removing elements from the
 * buffer.
 * - Option to allow or disallow duplicate elements.
 * - Thread-safe utility methods for checking buffer capacity, size, and
 * emptiness.
 * - Clear method to reset the buffer state.
 *
 * @tparam T The type of elements stored in the buffer.
 */
#ifndef DOXYGEN_SHOULD_SKIP_THIS
template <typename T, typename = void>
struct has_equal_operator : std::false_type {};

template <typename T>
struct has_equal_operator<
    T, std::void_t<decltype(std::declval<T>() == std::declval<T>())>>
    : std::true_type {};

template <typename T>
constexpr bool has_equal_operator_v = has_equal_operator<T>::value;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */
template <typename T>
class CircularBuffer {
 public:
  /* 目前没有动态调整策略的需求，暂时采用静态策略 ，后续有需要再添加动态调整策略
   */
  static constexpr bool AllowDuplicates = true;  // 是否允许缓冲区中的元素重复
  static constexpr bool LimitSize = false;       // 是否限制缓冲区大小
  static constexpr bool Replace_oldest = false;  // 满时是否替换最旧的元素

  using value_type = T;  // 添加 value_type 定义，以便在其他地方使用
  explicit CircularBuffer(size_t capacity = 0)
      : m_capacity(capacity), m_head(0), m_tail(0), m_size(0) {
    reserve(m_capacity);
  }
  ~CircularBuffer() = default;
  /**
   * @brief 向循环缓冲区添加元素（线程安全）
   *
   * @param t 要添加的元素的指针类型。
   * @return 如果成功添加元素，返回 true；否则返回 false
   */
  bool push(T* t) {
    if (t == nullptr) {
      return false;
    }
    return _push_internal_(*t);
  }
  /**
   * @brief 向循环缓冲区添加元素（线程安全）
   *
   * @tparam U 元素类型
   * @param t 要添加的元素（右值引用）,可以是左值引用或右值引用。
   * @return 如果成功添加元素，返回 true；否则返回 false
   */
  template <typename U>
  bool push(U&& t) {
    return _push_internal_(std::forward<U>(t));
  }

  /**
   * @brief 添加一个元素到循环缓冲区中，使用构造函数参数
   *
   * @tparam Args 传递给 T 构造函数的参数类型
   * @param args 传递给 T 构造函数的参数
   * @return 如果元素成功添加到循环缓冲区，返回 true；否则返回 false
   */
  template <typename... Args>
  bool emplace(Args&&... args) {
    T t(std::forward<Args>(args)...);
    return _push_internal_(std::move(t));
  }
  /**
   * 尝试从循环缓冲区中弹出一个元素。
   * @decide: 该函数将立即返回，不会等待。
   * @param t 引用，用于保存弹出的元素
   * @return 如果成功弹出元素，返回 true；否则返回 false
   */
  bool pop(T& t) {
    return _pop_internal_(t, std::chrono::microseconds::zero());
  }
  /**
   * 在指定的超时时间内等待并尝试从循环缓冲区中弹出一个元素。
   *
   * @tparam Rep 表示时间的整数类型。
   * @tparam Period 表示时间单位的比率类型。
   * @param[out] t 用于存储弹出元素的引用。
   * @param[in] timeout 等待超时时间，默认为 -1 微妙（无限等待）。
   * @return 如果成功从缓冲区中弹出元素，则返回 true，否则返回 false。
   */
  template <typename Duration = std::chrono::microseconds>
  bool wait_and_pop(T& t,
                    const Duration& timeout = std::chrono::microseconds(-1)) {
    return _pop_internal_(t, timeout);
  }
  /**
   * @brief 获取当前循环缓冲区的容量
   * @return 当前循环缓冲区的容量
   */
  size_t capacity() const {
    std::unique_lock<std::mutex> lock(m_mutex);
    return m_capacity;
  }
  /**
   * @brief 判断循环缓冲区是否为空
   * @return 如果循环缓冲区为空返回 true，否则返回 false
   */
  bool empty() const {
    std::unique_lock<std::mutex> lock(m_mutex);
    return m_size == 0;
  }

  /**
   * @brief 调整缓冲区的容量
   * @param new_capacity 新的缓冲区容量
   */
  void reserve(size_t new_capacity) {
    std::unique_lock<std::mutex> lock(m_mutex);
    _resize_(new_capacity);
  }

  /**
   * @brief 获取循环缓冲区中元素的数量
   * @return 循环缓冲区中元素的数量
   */
  size_t size() const {
    std::unique_lock<std::mutex> lock(m_mutex);
    return m_size;
  }
  /**
   * @brief 清空缓冲区，移除所有元素
   */
  void clear() {
    std::unique_lock<std::mutex> lock(m_mutex);
    m_head = 0;
    m_tail = 0;
    m_size = 0;
  }

 private:
  size_t m_capacity;              // 缓冲区的容量
  std::vector<T> m_buffer;        // 存储元素的缓冲区
  size_t m_head, m_tail, m_size;  // 头部、尾部和缓冲区的大小
  mutable std::mutex m_mutex;  // 互斥锁，用于线程安全访问缓冲区
  std::condition_variable m_cv;  // 条件变量，用于线程间通信

  /**
   * @brief 内部函数，向缓冲区添加元素（线程安全）
   *
   * 该函数尝试将元素添加到缓冲区。如果设置了不允许重复元素，且元素已存在于缓冲区，函数将返回
   * false。 如果设置了限制缓冲区大小，且缓冲区已满，函数将返回 false。
   * 如果没有限制大小，且缓冲区已满，函数将扩大缓冲区容量。
   *
   * @param t 要添加的元素
   * @return 如果成功添加元素，返回 true；否则返回 false
   */
  template <typename U>
  bool _push_internal_(U&& t) {
    // 使用互斥锁来确保线程安全
    std::unique_lock<std::mutex> lock(m_mutex);
    if constexpr (!AllowDuplicates) {
      if constexpr (has_equal_operator_v<T>) {
        for (const auto& elem : m_buffer) {
          if (elem == t) {
            return false;
          }
        }
      } else {
        //   std::cout <<"not deal qual " <<std::endl;
      }
    }
    if constexpr (LimitSize) {
      if (m_size >= m_capacity) return false;
    } else if (m_size >= m_capacity) {
      _resize_(m_capacity * 2);
    }
    // 将元素添加到缓冲区的尾部
    if constexpr (std::is_pointer<U>::value) {
      m_buffer[m_tail] = *t;
    } else {
      m_buffer[m_tail] = std::forward<U>(t);
    }
    // 更新循环缓冲区的尾部和大小
    m_tail = (m_tail + 1) % m_capacity;
    ++m_size;

    // 释放锁
    lock.unlock();

    // 唤醒等待线程
    m_cv.notify_one();

    // 如果成功将元素添加到缓冲区，返回 true
    return true;
  }
  /**
   * @brief 内部函数，尝试从缓冲区中弹出一个元素，可设置超时时间
   *
   * @tparam Duration 时间单位，默认为微秒
   * @param[out] t 用于存储弹出元素的引用
   * @param[in] timeout 等待超时时间，默认为 0 微秒（立即返回）
   * @return 如果成功从缓冲区中弹出元素，则返回 true，否则返回 false
   *
   * 当 timeout 为零时，函数将立即尝试获取数据，不会等待。
   * 当 timeout 大于零时，函数将最多尝试等待指定的超时时间。
   * 如果在超时时间内循环缓冲区中仍无元素可弹出，函数将立即返回 false。
   */
  template <typename Duration = std::chrono::microseconds>
  bool _pop_internal_(T& t, const Duration& timeout) {
    // 使用互斥锁来确保线程安全
    std::unique_lock<std::mutex> lock(m_mutex);

    if (m_size == 0) {
      // 如果超时时间为零且缓冲区为空，立即返回 false
      if (timeout == Duration::zero()) {
        return false;
      }
      // 如果超时时间小于零，等待直到缓冲区中有元素
      else if (timeout < Duration::zero()) {
        m_cv.wait(lock, [this]() { return m_size > 0; });
      }
      // 如果超时时间大于零，等待指定的超时时间，如果超时且缓冲区仍为空，返回
      // false
      else {
        if (!m_cv.wait_for(lock, timeout, [this]() { return m_size > 0; })) {
          return false;
        }
      }
      if (m_size == 0) {
        return false;
      }
    }
    // 将缓冲区的首个元素移动到给定引用
    t = std::move(m_buffer[m_head]);
    // 更新循环缓冲区的头部和大小
    m_head = (m_head + 1) % m_capacity;
    --m_size;
    // 如果成功弹出元素，返回 true
    return true;
  }
  /**
   * @brief 调整循环缓冲区的容量
   *
   * 如果新容量等于当前容量，则不进行任何操作。
   * 否则，将元素移动到新缓冲区中，保留最新的元素并移除最旧的元素。
   * 最后更新头、尾、容量和缓冲区。
   *
   * @param new_capacity 新的缓冲区容量
   */
  void _resize_(size_t new_capacity);
};
template <typename T>
void CircularBuffer<T>::_resize_(size_t new_capacity) {
  // if capacity is not changed, do nothing
  if ((new_capacity == m_capacity || new_capacity <= 0) &&
      (m_buffer.size() > 0)) {
    return;
  }
  size_t desiredCapacity = std::max(new_capacity, static_cast<size_t>(1));
  // 检查 T 是否有默认构造函数

  if constexpr (!std::is_default_constructible<T>::value) {
    // 对没有默认构造函数的类型使用 std::aligned_alloc 和定位 new
    T* new_buffer = static_cast<T*>(operator new(desiredCapacity * sizeof(T), std::align_val_t(alignof(T))));


    // 将元素从旧缓冲区移动到新缓冲区
    for (size_t i = 0; i < std::min(m_size, desiredCapacity); ++i) {
      new (&new_buffer[i]) T(std::move(m_buffer[i]));  // 定位 new，使用移动构造
      m_buffer[i].~T();                                // 调用析构函数
    }

    // 释放旧缓冲区
    std::free(m_buffer);
    m_buffer = new_buffer;
    m_capacity = desiredCapacity;
    m_head = 0;
    m_tail = m_size;
  } else {
    try {
      // if buffer is empty, resize the buffer
      if (m_buffer.size() == 0) {
        m_buffer.resize(desiredCapacity);
        m_capacity = desiredCapacity;
      } else {  // if buffer is not empty, move the elements to the new buffer
        std::vector<T> new_buffer(desiredCapacity);
        size_t new_tail = 0;

        // 从尾部开始遍历，保留最新的元素，移除最旧的元素
        size_t elements_to_copy = std::min(m_size, desiredCapacity);
        size_t old_tail = (m_tail + m_capacity - 1) % m_capacity;
        for (size_t j = 0; j < elements_to_copy; ++j) {
          size_t old_index = (old_tail + m_capacity - j) % m_capacity;
          size_t new_index = (desiredCapacity - j - 1) % desiredCapacity;

          new_buffer[new_index] = std::move(m_buffer[old_index]);
          new_tail = (new_tail + 1) % desiredCapacity;
        }
        // 更新头、尾、容量和缓冲区
        m_capacity = desiredCapacity;
        m_buffer = std::move(new_buffer);
        m_head = new_tail;
        m_tail = 0;
        m_size = elements_to_copy;
      }
    } catch (const std::bad_alloc& e) {
      std::cerr << "Memory allocation failed: " << e.what() << std::endl;
      // 你可以在这里添加更多的错误处理代码，例如回滚操作，或者重新抛出异常
    } catch (const std::exception& e) {
      // 捕获其他类型的标准异常
      std::cerr << "Exception: " << e.what() << std::endl;
    } catch (...) {
      // 捕获所有其他类型的异常
      std::cerr << "Unknown exception caught" << std::endl;
    }
  }
}
