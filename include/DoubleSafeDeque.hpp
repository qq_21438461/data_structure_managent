/*
 * SafeDeque is a thread-safe, double-ended queue (deque) implementation for C++.
 * It is based on the standard deque container, but provides additional synchronization
 * mechanisms such as mutexes, condition variables, and capacity control to ensure
 * proper functioning in a multi-threaded environment.
 *
 * Features:
 * 1. Thread-safe push, pop, and other deque operations.
 * 2. Capacity control with optional blocking on capacity limit.
 * 3. Allow or disallow duplicate elements in the deque.
 * 4. Non-blocking try-push and try-pop operations with configurable timeout.
 * 5. Thread-safe size, empty, and clear methods.
 *
 * Usage:
 * Simply include this header file in your project and use the SafeDeque class template
 * as a replacement for the standard deque. The template parameter T represents the type
 * of elements that the deque will store.
 *
 * Example:
 * SafeDeque<int> safeDeque;
 * safeDeque.push_back(42);
 * int value;
 * safeDeque.pop_front(value);
 */
#include <iostream>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <algorithm>

using namespace std;

template<typename T>
class SafeDeque {
public:
    SafeDeque(size_t capacity = 0, bool allow_duplicates = true)
        : m_capacity(capacity), m_allow_duplicates_(allow_duplicates) {
    }
    bool push_back(const T& t) {
        return push(t, true);
    }

    bool push_front(const T& t) {
        return push(t, false);
    }
	
	bool try_push_front(const T& t) {
		return try_push(t, false);
	}

	bool try_push_back(const T& t) {
		return try_push(t, true);
	}
	bool try_push_front(T&& t) {
		return try_push(std::move(t), false);
	}

	bool try_push_back(T&& t) {
		return try_push(std::move(t), true);
	}

    bool emplace_back(T&& t) {
        return emplace(std::move(t), true);
    }

    bool emplace_front(T&& t) {
        return emplace(std::move(t), false);
    }

    bool pop_back(T& t) {
        return pop(t, true);
    }

    bool pop_front(T& t) {
        return pop(t, false);
    }

    bool wait_pop_back(T& t) {
        return wait_pop(t, true);
    }

    bool wait_pop_front(T& t) {
        return wait_pop(t, false);
    }

    bool empty() const {
        unique_lock<mutex> lock(m_mutex);
        return m_queue.empty();
    }

    size_t size() const {
        unique_lock<mutex> lock(m_mutex);
        return m_queue.size();
    }

    void set_allow_duplicates(bool allow_duplicates) {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_allow_duplicates_ = allow_duplicates;
    }
    
    void clear() {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_queue.clear();
      m_cv_push.notify_all();
    }
    
    void set_capacity(size_t capacity) {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_capacity = capacity;
        lock.unlock();
        m_cv_push.notify_all();
    }

private:
    std::deque<T> m_queue;
    mutable std::mutex m_mutex;
    std::condition_variable m_cv_pop;
    size_t m_capacity = 0;
    bool m_allow_duplicates_;
    std::condition_variable m_cv_push;

    template<typename U>
    bool push(U&& t, bool back = true) {
        std::unique_lock<mutex> lock(m_mutex);
        if (m_capacity > 0 && m_queue.size() >= m_capacity) {
            return false;
        }
		 m_cv_push.wait(lock, [this](){ return m_queue.size() < m_capacity || m_capacity == 0; });


        if (!m_allow_duplicates_ && std::find(m_queue.cbegin(), m_queue.cend(), t) != m_queue.cend()) {
            return false;
        }

        if (back) {
            m_queue.push_back(std::forward<U>(t));
        } else {
            m_queue.push_front(std::forward<U>(t));
        }
        lock.unlock();
        m_cv_pop.notify_one();
        return true;
    }

    template<typename... Args>
    bool emplace(Args&&... args, bool back = true) {
        std::unique_lock<std::mutex> lock(m_mutex);
		if (m_capacity > 0 && m_queue.size() >= m_capacity) {
			return false;
		}

        T t(std::forward<Args>(args)...);
        if (!m_allow_duplicates_ && std::find(m_queue.cbegin(), m_queue.cend(), t) != m_queue.cend()) {
            return false;
        }
        if (back) {
            m_queue.push_back(std::move(t));
        } else {
            m_queue.push_front(std::move(t));
        }
        lock.unlock();
        m_cv_pop.notify_one();
        return true;
    }

    template<typename Rep = int64_t, typename Period = std::ratio<1>>
    bool try_push(T&& t, bool back = true, const std::chrono::duration<Rep, Period>& timeout = std::chrono::nanoseconds(-1)) {
        std::unique_lock<mutex> lock(m_mutex);
		
		if (timeout.count() < std::chrono::duration<Rep, Period>::zero()) {
			// 无限等待
			m_cv_push.wait(lock, [this]() { return m_queue.size() < m_capacity; });
		} else {
			// 有限等待
			if (!m_cv_push.wait_for(lock, timeout, [this]() { return m_queue.size() < m_capacity; })) {
				return false;
			}
		}

        if (!m_allow_duplicates_ && std::find(m_queue.cbegin(), m_queue.cend(), t) != m_queue.cend()) {
            return false;
        }

        if (back) {
            m_queue.push_back(std::forward<T>(t));
        } else {
            m_queue.push_front(std::forward<T>(t));
        }
        lock.unlock();
        m_cv_pop.notify_one();
        return true;
    }

    template<typename Rep = int64_t, typename Period = std::ratio<1>>
    bool wait_pop(T& t, bool back = true, const std::chrono::duration<Rep, Period>& timeout = std::chrono::nanoseconds(-1)) {
        std::unique_lock<mutex> lock(m_mutex);

        if (timeout.count() < std::chrono::duration<Rep, Period>::zero()){
            m_cv_pop.wait(lock, [this]() { return !m_queue.empty(); });
        } else {
            if (!m_cv_pop.wait_for(lock, timeout, [this]() { return !m_queue.empty(); })) {
                return false;
            }
        }

        if (back) {
            t = std::move(m_queue.back());
            m_queue.pop_back();
        } else {
            t = std::move(m_queue.front());
            m_queue.pop_front();
        }
        lock.unlock();
        m_cv_push.notify_one();
        return true;
    }
    
};

