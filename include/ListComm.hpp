/**
 * @file ListComm.hpp
 * @author liuzhiyu (liuzhiyu@inalfa-acms.com)
 * @brief This file defines the SafeList class.
 * @date 2023-12-15
 * @version 1.0.0
 * @copyright Copyright (c) 2023 by liuzhiyu, All Rights Reserved.
 */
#pragma once
#include <algorithm>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <list>
#include <mutex>
#include <thread>

template <typename T>
class SafeList {
 public:
  using value_type = T;
  SafeList() = default;
  ~SafeList() = default;
  /**
   * @brief 向列表添加元素（线程安全）
   *
   * 本函数接受一个指针类型的参数，并尝试将其指向的元素添加到列表中。
   * 如果传入的指针为空（nullptr），则不会尝试添加，函数返回 false。
   * 使用模板函数 _push_internal_ 实现添加操作。
   *
   * @param t 要添加的元素的指针类型。
   * @return 如果成功添加元素，返回 true；否则返回 false
   */
  bool push(T* t) {
    if (t == nullptr) {
      return false;
    }
    return _push_internal_(*t);
  }
  /**
   * @brief 向列表添加元素（线程安全）
   *
   * 本函数接受一个模板类型的参数，并尝试将其添加到列表中。
   * 参数可以是左值引用或右值引用。
   * 使用模板函数 _push_internal_ 实现添加操作。
   *
   * @param t 要添加的元素的模板类型。
   * @return 如果成功添加元素，返回 true；否则返回 false
   */
  template <typename U>
  bool push(U&& t) {
    return _push_internal_(std::forward<U>(t));
  }
  /**
   * @brief 向列表添加元素（线程安全）
   *
   * 本函数接受多个参数，并在列表中构造一个新的元素。
   * 使用模板函数 _emplace_internal_ 实现添加操作。
   *
   * @tparam Args 参数类型
   * @param args 要传递给元素构造函数的参数
   * @return 如果成功添加元素，返回 true；否则返回 false
   */
  template <typename... Args>
  bool emplace(Args&&... args) {
    T t(std::forward<Args>(args)...);
    return _push_internal_(std::move(t));
  }
  /**
   * 尝试从链表中弹出一个元素。
   * @decide: 该函数将立即返回，不会等待。
   * @param t 引用，用于保存弹出的元素
   * @return 如果成功弹出元素，返回 true；否则返回 false
   */
  bool pop(T& t) {
    return _pop_internal_(t, std::chrono::microseconds::zero());
  }
  /**
   * 尝试等待并弹出链表中的元素。
   * 如果在给定的超时时间内链表为空，则返回 false，否则返回 true
   * 并将弹出的元素存储在 t 中。
   *
   * @tparam Rep 表示时间的数量类型。
   * @tparam Period 时间单位类型。
   * @param[out] t 用于存储弹出的元素。
   * @param[in] timeout 等待队列非空的超时时间。默认值为 -1，表示无限等待。
   * @return 如果在超时时间内成功弹出元素，则返回 true，否则返回 false。
   */
  template <typename Duration = std::chrono::microseconds>
  bool wait_and_pop(T& t,
                    const Duration& timeout = std::chrono::microseconds(-1)) {
    return _pop_internal_(t, timeout);
  }
  /**
   * @brief 检查列表是否为空
   *
   * @return 如果列表为空，返回 true；否则返回 false
   */
  bool empty() const {
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_list.empty();
  }
  /**
   * @brief 清空列表
   *
   * 清空列表中的所有元素。
   */
  void clear() {
    std::unique_lock lock(m_mutex);
    m_list.clear();
  }
  /**
   * @brief 获取列表中的元素数量
   *
   * @return 列表中的元素数量
   */
  size_t size() const {
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_list.size();
  }

 private:
  template <typename U>
  bool _push_internal_(U&& t) {
    std::unique_lock<std::mutex> lock(m_mutex);
    if constexpr (std::is_pointer<U>::value) {
      // 如果 U 是指针类型，解引用 t
      m_list.push_back(*t);
    } else {
      m_list.push_back(std::forward<U>(t));
    }
    m_cv.notify_one();
    return true;
  }
  bool _pop_internal_(T& t, const std::chrono::microseconds& timeout) {
    std::unique_lock<std::mutex> lock(m_mutex);
    if (m_list.empty()) {
      // 根据超时设置等待
      if (timeout.count() < 0) {
        m_cv.wait(lock, [this]() { return !m_list.empty(); });
      } else if (timeout.count() == 0) {
        return false;
      } else if (!m_cv.wait_for(lock, timeout,
                                [this]() { return !m_list.empty(); })) {
        return false;
      }
    }
    t = std::move(m_list.front());
    m_list.pop_front();
    return true;
  }

 private:
  mutable std::mutex m_mutex;
  std::condition_variable m_cv;
  std::list<T> m_list;
};
