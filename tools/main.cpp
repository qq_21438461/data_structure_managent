#include "../include/CircularBuffer.hpp"
#include "../include/CircularList.hpp"
#include <iostream>
#include <string>
#include <map>
struct A
{
    int a;
    int b;
    int c;
    std::string hh;
    std::string vv;
};

int main(int argc, char** argv)
{
    CircularBuffer<struct  A> buffer(10);
    struct A a;
    a.a = 1;
    a.b = 2;
    a.c = 3;
    a.hh = "Hello";
    a.vv = "World";
    buffer.push(a);

    CircularBuffer<int> buffer_int(10);
    buffer_int.push(1);
    buffer_int.push(2);
    CircularBuffer<std::map<int, int>> buffer_map(10);
    std::map<int, int> map;
    map[1] = 1;
    map[2] = 2;
    buffer_map.push(map);
    

    
    // buffer.push("World");
    // buffer.push("!");


    return 0;
}
